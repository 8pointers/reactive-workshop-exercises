import { useState } from 'react';
import { createRoot } from 'react-dom/client';

const useReducer = (reducer, initialState) => {
  const [state, setState] = useState(initialState);
  return [state, (action) => setState(reducer(state, action))];
};

const reducer = (state, action) => (action.type === 'INCREMENT' ? state + 1 : state);

const Counter = () => {
  const [count, dispatch] = useReducer(reducer, 0);
  return (
    <>
      <button onClick={() => dispatch({ type: 'INCREMENT' })}>+</button>
      {count}
    </>
  );
};

createRoot(document.getElementById('root-4')!).render(<Counter />);
