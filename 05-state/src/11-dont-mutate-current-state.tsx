import { memo, Component, PureComponent } from 'react';
import { createRoot } from 'react-dom/client';

class Friends1 extends Component<{ friends: string[] }> {
  render() {
    console.log('Friends1');
    return (
      <ul>
        {this.props.friends.map((name) => (
          <li>{name}</li>
        ))}
      </ul>
    );
  }
}

class Friends2 extends PureComponent<{ friends: string[] }> {
  render() {
    console.log('Friends2');
    return (
      <ul>
        {this.props.friends.map((name) => (
          <li>{name}</li>
        ))}
      </ul>
    );
  }
}

const Friends3 = memo(({ friends }: { friends: string[] }) => {
  console.log('Friends3');
  return (
    <ul>
      {friends.map((name) => (
        <li>{name}</li>
      ))}
    </ul>
  );
});

class AddressBook extends Component<{}, { value: number; friends: string[] }> {
  state = { value: 0, friends: [] };
  increment = () => this.setState(({ value }) => ({ value: value + 1 }));
  add1 = () => {
    this.setState({
      friends: [...this.state.friends, `Friend ${this.state.friends.length}`],
    });
  };
  add2 = () => {
    const state = this.state;
    state.friends.push(`Friend ${state.friends.length}`);
    this.setState(state);
  };
  render() {
    return (
      <div>
        <button onClick={this.increment}>{this.state.value}</button>
        <button onClick={this.add1}>Add friend 1</button>
        <button onClick={this.add2}>Add friend 2</button>
        <Friends1 friends={this.state.friends} />
        <Friends2 friends={this.state.friends} />
        <Friends3 friends={this.state.friends} />
      </div>
    );
  }
}

createRoot(document.getElementById('root-11')!).render(<AddressBook />);
