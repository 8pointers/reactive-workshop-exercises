import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Counter extends Component<{}, { value: number }> {
  state = { value: 0 };

  increment = () => {
    this.setState((state) => ({ value: state.value + 1 }));
    this.setState((state) => ({ value: state.value + 1 }));
  };

  render = () => (
    <>
      <button onClick={this.increment}>+</button>
      {this.state.value}
    </>
  );
}

createRoot(document.getElementById('root-9')!).render(<Counter />);
