import { useReducer } from 'react';
import { createRoot } from 'react-dom/client';

const useState = (initialState) => useReducer((_, action) => action, initialState);

const Counter = () => {
  const [count, setCount] = useState(0);
  return (
    <>
      <button onClick={() => setCount(count + 1)}>+</button>
      {count}
    </>
  );
};

createRoot(document.getElementById('root-3')!).render(<Counter />);
