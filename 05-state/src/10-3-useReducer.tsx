import { useReducer } from 'react';
import { createRoot } from 'react-dom/client';

const useStateWithMerge = (initialState) =>
  useReducer((state, action) => ({ ...state, ...action }), initialState);

const Counter = () => {
  const [{ count, isVisible }, setState] = useStateWithMerge({
    count: 0,
    isVisible: true,
  });
  return (
    <div>
      <button onClick={() => setState({ isVisible: !isVisible })}>Toggle</button>
      {isVisible && (
        <div>
          <button onClick={() => setState({ count: count + 1 })}>+</button>
          {count}
        </div>
      )}
    </div>
  );
};

createRoot(document.getElementById('root-10-3')!).render(<Counter />);
