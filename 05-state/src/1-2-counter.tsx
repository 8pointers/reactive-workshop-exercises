import { useState } from 'react';
import { createRoot } from 'react-dom/client';

const Counter = () => {
  const [count, setCount] = useState(0);
  return (
    <>
      <button onClick={() => setCount(count + 1)}>+</button>
      {count}
    </>
  );
};

createRoot(document.getElementById('root-1-2')!).render(<Counter />);
