import { useReducer } from 'react';
import { createRoot } from 'react-dom/client';

const initialState = { count: 0, isVisible: true };
const reducer = (
  state: { count: number; isVisible: boolean },
  action: { type: string },
) => {
  if (action.type === 'INCREMENT') {
    return { ...state, count: state.count + 1 };
  } else if (action.type === 'TOGGLE') {
    return { ...state, isVisible: !state.isVisible };
  }
  return state;
};

const Counter = () => {
  const [{ count, isVisible }, dispatch] = useReducer(reducer, initialState);
  return (
    <div>
      <button onClick={() => dispatch({ type: 'TOGGLE' })}>Toggle</button>
      {isVisible && (
        <div>
          <button onClick={() => dispatch({ type: 'INCREMENT' })}>+</button>
          {count}
        </div>
      )}
    </div>
  );
};

createRoot(document.getElementById('root-10-2')!).render(<Counter />);
