import { createRoot } from 'react-dom/client';

let count = 0;

const Counter = () => (
  <>
    <button onClick={() => count++}>+</button>
    {count}
  </>
);

const root = createRoot(document.getElementById('root-1-1')!);
root.render(<Counter />);
// setInterval(() => root.render(<Counter />, 5000);
