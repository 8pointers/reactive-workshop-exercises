import { useReducer } from 'react';
import { createRoot } from 'react-dom/client';

const reducer = (state: number, action: { type: string }) =>
  action.type === 'INCREMENT' ? state + 1 : state;

const Counter = () => {
  const [count, dispatch] = useReducer(reducer, 0);
  return (
    <>
      <button onClick={() => dispatch({ type: 'INCREMENT' })}>+</button>
      {count}
    </>
  );
};

createRoot(document.getElementById('root-2')!).render(<Counter />);
