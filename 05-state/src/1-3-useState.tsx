import { useState } from 'react';
import { createRoot } from 'react-dom/client';

const Counter = () => {
  const [count, setCount] = useState(0);
  const [isVisible, setVisible] = useState(true);
  return (
    <div>
      <button onClick={() => setVisible((isVisible) => !isVisible)}>Toggle</button>
      {isVisible && (
        <div>
          <button onClick={() => setCount(count + 1)}>+</button>
          {count}
        </div>
      )}
    </div>
  );
};

createRoot(document.getElementById('root-1-3')!).render(<Counter />);
