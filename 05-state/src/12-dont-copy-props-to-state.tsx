import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Counter1 extends Component<{ offset: number }, { value: number }> {
  state = { value: 0 };
  increment = () => this.setState(({ value }) => ({ value: value + 1 }));
  render = () => (
    <button onClick={this.increment}>{this.props.offset + this.state.value}</button>
  );
}

class Counter2 extends Component<
  { offset: number },
  { offset: number; value: number }
> {
  constructor(props) {
    super(props);
    this.state = {
      offset: props.offset,
      value: 0,
    };
  }
  increment = () => this.setState(({ value }) => ({ value: value + 1 }));
  render = () => (
    <button onClick={this.increment}>{this.state.offset + this.state.value}</button>
  );
}

class Counters extends Component<{}, { value: number }> {
  state = { value: 0 };
  increment = () => this.setState(({ value }) => ({ value: value + 1 }));
  render = () => (
    <div>
      Offset: <button onClick={this.increment}>{this.state.value}</button>
      Counter1: <Counter1 offset={this.state.value} />
      Counter2: <Counter2 offset={this.state.value} />
    </div>
  );
}

createRoot(document.getElementById('root-12')!).render(<Counters />);
