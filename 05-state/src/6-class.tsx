import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Counter extends Component {
  state = { value: 0 };

  increment = () => this.setState({ value: this.state.value + 1 });

  render = () => (
    <>
      <button onClick={this.increment}>+</button>
      {this.state.value}
    </>
  );
}

createRoot(document.getElementById('root-6')!).render(<Counter />);
