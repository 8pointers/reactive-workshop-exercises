import classnames from 'classnames';
import './game-of-life.css';

const GameOfLife = ({ n = 10, width = 20, height = 20 }) => {
  const toggleCellState = (row, column) =>
    console.log('toggleCellState', row, column);
  const tick = () => console.log('tick');
  return (
    <div>
      <div style={{ width: n * width, height: n * height }}>
        {Array.from({ length: n * n })
          .map((_, index) => ({ row: index % n, column: Math.floor(index / n) }))
          .map(({ row, column }) => (
            <div
              onClick={() => toggleCellState(row, column)}
              className={classnames({ cell: true, alive: (row + column) % 2 })}
              style={{ top: 20 * row, left: 20 * column, width, height }}
            />
          ))}
      </div>
      <button onClick={tick}>Tick</button>
    </div>
  );
};
export default GameOfLife;
