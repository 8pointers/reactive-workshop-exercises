import { useState } from 'react';
import { createRoot } from 'react-dom/client';

const useCounter = (): [number, () => void] => {
  const [count, setValue] = useState(0);
  return [count, () => setValue(count + 1)];
};

const Counter = () => {
  const [count, increment] = useCounter();
  return (
    <>
      <button onClick={increment}>+</button>
      {count}
    </>
  );
};

createRoot(document.getElementById('root-5')!).render(<Counter />);
