import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Counter extends Component {
  state = {
    isVisible: true,
    count: 0,
  };
  render = () => {
    const { count, isVisible } = this.state;
    return (
      <div>
        <button onClick={() => this.setState({ isVisible: !isVisible })}>
          Toggle
        </button>
        {this.state.isVisible && (
          <div>
            <button onClick={() => this.setState({ count: count + 1 })}>+</button>
            {count}
          </div>
        )}
      </div>
    );
  };
}

createRoot(document.getElementById('root-10-1')!).render(<Counter />);
