const Alerter = () => {
  const showMessage = () => alert('Clicked!');
  return <button onClick={showMessage}>Click me</button>;
};

export default Alerter;
