## Exercise

Refactor the GameOfLife component so that when each cell is clicked it logs (using `console.log`) its position (row and column). When a _Tick_ button is clicked, it should log `"Computing the next state of the game..."`.
