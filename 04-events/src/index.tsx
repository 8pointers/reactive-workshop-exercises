import { createRoot } from 'react-dom/client';

import './index.css';
import Alerter1 from './1-onClick-1';
import Alerter2 from './1-onClick-2';
import Alerter3 from './2-problem';
import Alerter4 from './3-fixed-arrow';
import Alerter5 from './3-fixed-bind-1';
import Alerter6 from './3-fixed-bind-2';
import Alerter7 from './3-fixed-es2017';

import './ex-1';

createRoot(document.getElementById('root-1-1')!).render(<Alerter1 />);
createRoot(document.getElementById('root-1-2')!).render(<Alerter2 />);
createRoot(document.getElementById('root-2')!).render(<Alerter3 />);
createRoot(document.getElementById('root-3-1')!).render(<Alerter4 />);
createRoot(document.getElementById('root-3-2')!).render(<Alerter5 />);
createRoot(document.getElementById('root-3-3')!).render(<Alerter6 />);
createRoot(document.getElementById('root-3-4')!).render(<Alerter7 />);
