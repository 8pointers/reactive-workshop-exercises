import { Component } from 'react';

class Alerter extends Component {
  prepare() {
    console.log('Preparing...');
  }
  showMessage() {
    this.prepare();
    alert('Clicked!');
  }
  render() {
    return <button onClick={() => this.showMessage()}>Click me</button>;
  }
}

export default Alerter;
