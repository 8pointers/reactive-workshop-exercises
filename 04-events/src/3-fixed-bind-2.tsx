import { Component } from 'react';

class Alerter extends Component {
  constructor(props: {}) {
    super(props);
    this.showMessage = this.showMessage.bind(this);
  }
  prepare() {
    console.log('Preparing...');
  }
  showMessage() {
    this.prepare();
    alert('Clicked!');
  }
  render() {
    return <button onClick={this.showMessage}>Click me</button>;
  }
}

export default Alerter;
