import { createRoot } from 'react-dom/client';
import { createSlice, configureStore } from '@reduxjs/toolkit';
import { useSelector, useDispatch, Provider } from 'react-redux';

const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
  },
});
const togglerSlice = createSlice({
  name: 'toggler',
  initialState: {
    value: true,
  },
  reducers: {
    toggle: (state) => {
      state.value = !state.value;
    },
  },
});
const store = configureStore({
  reducer: {
    counter: counterSlice.reducer,
    toggler: togglerSlice.reducer,
  },
});
const { increment } = counterSlice.actions;
const Counter = () => {
  console.log('Counter');
  const count = useSelector<{ counter: { value: number } }, number>(
    (state) => state.counter.value,
  );
  const dispatch = useDispatch();
  return <button onClick={() => dispatch(increment())}>{count}</button>;
};
const { toggle } = togglerSlice.actions;
const Toggler = () => {
  console.log('Toggler');
  const flag = useSelector<{ toggler: { value: number } }, number>(
    (state) => state.toggler.value,
  );
  const dispatch = useDispatch();
  return <button onClick={() => dispatch(toggle())}>{'' + flag}</button>;
};

createRoot(document.getElementById('root-4')!).render(
  <Provider store={store}>
    <div>
      <Counter />
      <Toggler />
    </div>
  </Provider>,
);
