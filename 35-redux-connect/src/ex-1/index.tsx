import { createRoot } from 'react-dom/client';
import './index.css';
import { createStore } from 'redux';
import gameOfLife from './game-of-life-reducer';
import GameOfLife from './game-of-life';

const root = createRoot(document.getElementById('root-ex-1')!);
const store = createStore(gameOfLife);
const renderApp = () =>
  root.render(
    <GameOfLife
      width={20}
      height={20}
      n={10}
      isAlive={store.getState()}
      onToggle={(row, column) =>
        store.dispatch({ type: 'TOGGLE_CELL_STATE', payload: { row, column } })
      }
      onTick={() => store.dispatch({ type: 'TICK' })}
    />,
  );
renderApp();
store.subscribe(renderApp);
