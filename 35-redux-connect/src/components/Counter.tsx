import { useSelector, useDispatch } from 'react-redux';
import counterSlice from '../reducers/counter';
import store from '../store/store';

const Counter = () => {
  const count = useSelector<ReturnType<typeof store.getState>, number>(
    (state) => state.counter.value,
  );
  const dispatch = useDispatch();
  return (
    <>
      {count}
      <button onClick={() => dispatch(counterSlice.actions.increment(1))}>+</button>
      <button onClick={() => dispatch(counterSlice.actions.increment(-1))}>-</button>
    </>
  );
};

export default Counter;
