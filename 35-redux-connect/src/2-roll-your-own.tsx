import { createContext, useContext } from 'react';
import { Store } from 'redux';

const ProviderContext = createContext<Store | undefined>(undefined);

const Provider = (props: { store: Store }) => (
  <ProviderContext.Provider value={props.store}></ProviderContext.Provider>
);

const useSelector = (selectorFn) =>
  selectorFn(useContext(ProviderContext).getState());

const useDispatch = () => useContext(ProviderContext).dispatch;

export { Provider, useSelector, useDispatch };
