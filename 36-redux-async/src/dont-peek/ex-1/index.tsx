import React from 'react';
import { createRoot } from 'react-dom/client';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './reducer';
import GameOfLife from './gameOfLife';

const store = createStore(reducer, applyMiddleware(thunk));

createRoot(document.getElementById('root-ex-1')!).render(
  <Provider store={store}>
    <GameOfLife width={20} height={20} n={10} />
  </Provider>,
);
