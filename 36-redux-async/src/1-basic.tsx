import { createRoot } from 'react-dom/client';
import { createAsyncThunk, createSlice, configureStore } from '@reduxjs/toolkit';
import { useSelector, useDispatch, Provider } from 'react-redux';

const fetchIp = createAsyncThunk('ip/fetchIp', () =>
  fetch('/api/ipify').then((response) => response.text()),
);
const ipSlice = createSlice({
  name: 'ip',
  initialState: {
    isFetching: false,
    address: '',
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchIp.pending, (state, action) => {
        state.isFetching = true;
      })
      .addCase(fetchIp.fulfilled, (state, action) => {
        state.isFetching = false;
        state.address = action.payload;
      });
  },
});
const store = configureStore({
  reducer: {
    ip: ipSlice.reducer,
  },
});
type AppDispatch = typeof store.dispatch;
type RootState = ReturnType<typeof store.getState>;
const IpAddress = () => {
  const ip = useSelector((state: RootState) => state.ip);
  const dispatch = useDispatch<AppDispatch>();
  return (
    <div>
      <button onClick={() => dispatch(fetchIp())}>Get my IP</button>
      {ip.isFetching ? 'Loading...' : ip.address}
    </div>
  );
};

createRoot(document.getElementById('root-1')).render(
  <Provider store={store}>
    <IpAddress />
  </Provider>,
);
