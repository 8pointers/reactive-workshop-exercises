import { createRoot } from 'react-dom/client';
import { createSlice, configureStore } from '@reduxjs/toolkit';
import { useSelector, useDispatch, Provider } from 'react-redux';

const ipSlice = createSlice({
  name: 'ip',
  initialState: {
    isFetching: false,
    address: '',
  },
  reducers: {
    request: (state) => {
      state.isFetching = true;
    },
    receive: (state, action) => {
      state.isFetching = false;
      state.address = action.payload;
    },
  },
});
const { request, receive } = ipSlice.actions;
const store = configureStore({
  reducer: {
    ip: ipSlice.reducer,
  },
});
type RootState = ReturnType<typeof store.getState>;
const IpAddress = () => {
  const ip = useSelector((state: RootState) => state.ip);
  const dispatch = useDispatch();
  return (
    <div>
      <button
        onClick={() => {
          dispatch(request());
          fetch('/api/ipify')
            .then((response) => response.text())
            .then((ipAddress) => dispatch(receive(ipAddress)));
        }}
      >
        Get my IP
      </button>
      {ip.isFetching ? 'Loading...' : ip.address}
    </div>
  );
};

createRoot(document.getElementById('root-0')).render(
  <Provider store={store}>
    <IpAddress />
  </Provider>,
);
