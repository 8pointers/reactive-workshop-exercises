import React from 'react';
import { createRoot } from 'react-dom/client';
import './game-of-life.css';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import gameOfLife from './game-of-life-reducer';
import GameOfLife from './game-of-life';

const store = createStore(gameOfLife);
createRoot(document.getElementById('root-ex-1')!).render(
  <Provider store={store}>
    <GameOfLife width={20} height={20} n={10} />
  </Provider>,
);
