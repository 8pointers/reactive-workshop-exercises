import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const MyIp = () => {
  const [address, setAddress] = useState('Loading...');
  useEffect(() => {
    fetch('/api/ipify')
      .then((response) => response.text())
      .then(setAddress);
  });
  console.log(new Date(), 'Rendering 2', address);
  return <div>{address}</div>;
};

createRoot(document.getElementById('root-2')!).render(<MyIp />);
