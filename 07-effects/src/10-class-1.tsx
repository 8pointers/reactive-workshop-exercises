import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class MyIp extends Component<{}, { address: string }> {
  state = { address: '' };

  componentDidMount = () =>
    fetch('/api/ipify')
      .then((response) => response.text())
      .then((address) => this.setState({ address }));

  render = () => <div>{this.state.address}</div>;
}

createRoot(document.getElementById('root-10-1')!).render(<MyIp />);
