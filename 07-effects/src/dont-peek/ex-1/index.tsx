import { Component, useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const CountdownFn = ({ to }: { to: Date }) => {
  const [remainingTime, setRemainingTime] = useState<number>();
  useEffect(() => {
    const interval = setInterval(
      () =>
        setRemainingTime(
          Math.max(0, Math.round((to.getTime() - Date.now()) / 1000)),
        ),
      1000,
    );
    return () => clearInterval(interval);
  }, [to]);
  return <div>{remainingTime || "Time's up!"}</div>;
};

class CountdownClass extends Component<{ to: Date }> {
  interval: any;
  state = { remainingTime: this.getRemainingTime() };

  getRemainingTime() {
    const now = new Date();
    return Math.max(0, Math.round((this.props.to.getTime() - now.getTime()) / 1000));
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState({ remainingTime: this.getRemainingTime() }),
      1000,
    );
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <div>{this.state.remainingTime || "Time's up!"}</div>;
  }
}

createRoot(document.getElementById('root-ex-1')!).render(
  <>
    <CountdownFn to={new Date(Date.now() + 5 * 1000)} />
    <CountdownClass to={new Date(Date.now() + 5 * 1000)} />
  </>,
);
