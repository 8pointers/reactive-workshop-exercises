import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const Clock = () => {
  const [now, setNow] = useState(new Date());
  useEffect(() => {
    const interval = setInterval(() => setNow(new Date()), 1000);
    return () => clearInterval(interval);
  }, []);
  return <div>{now.toLocaleTimeString()}</div>;
};

createRoot(document.getElementById('root-3-1')!).render(<Clock />);
