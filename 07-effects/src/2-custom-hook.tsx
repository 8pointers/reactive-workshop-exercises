import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const events = ['online', 'offline'];
const useOnline = () => {
  const [isOnline, setIsOnline] = useState(window.navigator.onLine);
  useEffect(() => {
    const listener = () => setIsOnline(window.navigator.onLine);
    events.forEach((t) => window.addEventListener(t, listener));
    return () => events.forEach((t) => window.removeEventListener(t, listener));
  }, []);
  return isOnline;
};

const App = () => {
  const isOnline = useOnline();
  return <div className="app">{isOnline ? '😀' : '🤕'}</div>;
};

createRoot(document.getElementById('root-2')!).render(<App />);
