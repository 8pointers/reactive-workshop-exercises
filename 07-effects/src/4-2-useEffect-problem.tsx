import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const Counter = () => {
  const [time, setTime] = useState(0);
  useEffect(() => {
    const timer = window.setInterval(() => setTime((time) => time + 1), 1000);
    return () => window.clearInterval(timer);
  }, []);
  return <span>{time}</span>;
};

const CounterController = () => {
  const [text, setText] = useState('Slowly type here...');
  return (
    <>
      4:
      <input
        type="text"
        value={text}
        onChange={(event) => setText(event.target.value)}
      />
      <Counter />
    </>
  );
};

createRoot(document.getElementById('root-4-2')!).render(<CounterController />);
