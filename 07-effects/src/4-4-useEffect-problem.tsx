import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const useInterval = (callback: () => void, period: number) => {
  useEffect(() => {
    const timer = window.setInterval(callback, period);
    return () => window.clearInterval(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [period]);
};

const Counter = ({ period }: { period: number }) => {
  const [time, setTime] = useState(0);
  useInterval(() => setTime(time + 1), period);
  return <span>{time}</span>;
};

const CounterController = () => {
  const [text, setText] = useState('Slowly type here...');
  const [period, setPeriod] = useState(1000);
  return (
    <>
      6:
      <input
        type="text"
        value={text}
        onChange={(event) => setText(event.target.value)}
      />
      <input
        type="number"
        value={period}
        onChange={(event) => setPeriod(+event.target.value)}
      />
      <Counter period={period} />
    </>
  );
};

createRoot(document.getElementById('root-4-4')!).render(<CounterController />);
