import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const Counter = ({ period }: { period: number }) => {
  const [time, setTime] = useState(0);
  useEffect(() => {
    const timer = window.setInterval(() => setTime((time) => time + 1), period);
    return () => window.clearInterval(timer);
  }, [period]);
  return <span>{time}</span>;
};

const CounterController = () => {
  const [text, setText] = useState('Slowly type here...');
  const [period, setPeriod] = useState(1000);
  return (
    <>
      5:
      <input
        type="text"
        value={text}
        onChange={(event) => setText(event.target.value)}
      />
      <input
        type="number"
        value={period}
        onChange={(event) => setPeriod(+event.target.value)}
      />
      <Counter period={period} />
    </>
  );
};

createRoot(document.getElementById('root-4-3')!).render(<CounterController />);
