import { ChangeEvent, useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const OnlineStatus = () => {
  const [isOnline, setIsOnline] = useState(window.navigator.onLine);
  useEffect(() => {
    const listener = () => setIsOnline(window.navigator.onLine);
    const eventTypes = ['online', 'offline'];
    console.log('Adding listeners...');
    eventTypes.forEach((t) => window.addEventListener(t, listener));
    return () => {
      console.log('Removing listeners...');
      eventTypes.forEach((t) => window.removeEventListener(t, listener));
    };
  });
  return <span>{isOnline ? '😀' : '🤕'}</span>;
};

const App = () => {
  const [text, setText] = useState('Keep typing...');
  const [isVisible, setIsVisible] = useState(false);
  const onChange = (event: ChangeEvent<HTMLInputElement>) =>
    setText(event.target.value);
  return (
    <div className="app">
      <input type="text" value={text} onChange={onChange} />
      <button onClick={() => setIsVisible(!isVisible)}>Toggle</button>
      {isVisible && <OnlineStatus />}
    </div>
  );
};

createRoot(document.getElementById('root-1-2')!).render(<App />);
