import { ChangeEvent, useState } from 'react';
import { createRoot } from 'react-dom/client';

const OnlineStatus = () => {
  console.log('Rendering...');
  const [isOnline, setIsOnline] = useState(window.navigator.onLine);
  const listener = () => setIsOnline(window.navigator.onLine);
  window.addEventListener('online', listener);
  window.addEventListener('offline', listener);
  return <span>{isOnline ? '😀' : '🤕'}</span>;
};

const App = () => {
  const [text, setText] = useState('Keep typing...');
  const [isVisible, setIsVisible] = useState(false);
  const onChange = (event: ChangeEvent<HTMLInputElement>) =>
    setText(event.target.value);
  return (
    <div className="app">
      <input type="text" value={text} onChange={onChange} />
      <button onClick={() => setIsVisible(!isVisible)}>Toggle</button>
      {isVisible && <OnlineStatus />}
    </div>
  );
};

createRoot(document.getElementById('root-1-1')!).render(<App />);
