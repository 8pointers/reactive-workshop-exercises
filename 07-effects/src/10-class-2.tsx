import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Clock extends Component {
  interval: any;
  state = { now: new Date() };

  componentDidMount = () =>
    (this.interval = setInterval(() => this.setState({ now: new Date() }), 1000));

  componentWillUnmount = () => clearInterval(this.interval);

  render = () => <div>{this.state.now.toLocaleTimeString()}</div>;
}

createRoot(document.getElementById('root-10-2')!).render(<Clock />);
