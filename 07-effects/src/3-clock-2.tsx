import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

const useClock = () => {
  const [now, setNow] = useState(new Date());
  useEffect(() => {
    const interval = setInterval(() => setNow(new Date()), 1000);
    return () => clearInterval(interval);
  }, []);
  return now;
};

const Clock = () => <div>{useClock().toLocaleTimeString()}</div>;

createRoot(document.getElementById('root-3-2')!).render(<Clock />);
