import { Component, ReactElement } from 'react';
import { createRoot } from 'react-dom/client';

class AutoRefresh extends Component<{
  interval: number;
  render: (Date) => ReactElement;
}> {
  interval: any;
  setupInterval = () =>
    (this.interval = setInterval(() => this.forceUpdate(), this.props.interval));
  componentDidMount = () => this.setupInterval();
  componentWillUnmount = () => clearInterval(this.interval);
  componentDidUpdate = (prevProps) => {
    if (this.props.interval !== prevProps.interval) {
      clearInterval(this.interval);
      this.setupInterval();
    }
  };
  render = () => this.props.render({ now: new Date() });
}

const App = () => (
  <AutoRefresh
    interval={1000}
    render={({ now }) => <div>The current time is: {now.toLocaleTimeString()}</div>}
  />
);

createRoot(document.getElementById('root-ex-1')!).render(<App />);
