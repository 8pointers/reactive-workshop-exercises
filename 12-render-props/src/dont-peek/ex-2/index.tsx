import { Component, ReactElement } from 'react';
import { createRoot } from 'react-dom/client';

class Fetch extends Component<
  {
    url: string;
    render: (props: { data?: string }) => ReactElement;
  },
  { data?: string }
> {
  state = {};
  async componentDidMount() {
    const response = await fetch(this.props.url);
    const data = await response.text();
    this.setState({ data });
  }
  render = () => this.props.render(this.state);
}

const App = () => (
  <div>
    <Fetch
      url="/api/ipify"
      render={({ data }) => <div>Your IP address is: {data}</div>}
    />
  </div>
);

createRoot(document.getElementById('root-ex-2')!).render(<App />);
