import { createRoot } from 'react-dom/client';
import { GameOfLifeContainer, GameOfLifePresentation } from './game-of-life.js';

createRoot(document.getElementById('root-ex-3')!).render(
  <GameOfLifeContainer
    n={10}
    width={20}
    height={20}
    render={GameOfLifePresentation}
  />
);
