import { Component, ReactElement } from 'react';
import { createRoot } from 'react-dom/client';

class Counter extends Component<
  {
    render: (props: { count: number; onIncrement: () => void }) => ReactElement;
  },
  { count: number }
> {
  state = { count: 0 };
  increment = () => this.setState(({ count }) => ({ count: count + 1 }));
  render = () => this.props.render({ ...this.state, onIncrement: this.increment });
}

const App = () => (
  <Counter
    render={({ count, onIncrement }) => (
      <button onClick={onIncrement}>{count}</button>
    )}
  />
);

createRoot(document.getElementById('root-1')!).render(<App />);
