import { createRoot } from 'react-dom/client';
import { createStore } from 'redux';

const Counter = ({ value, onIncrement }) => (
  <p>
    {value}
    <button onClick={onIncrement}>+</button>
  </p>
);

const store = createStore((state = 0, action) => {
  return action.type === 'INCREMENT' ? state + 1 : state;
});

const root = createRoot(document.getElementById('root'));
const renderApp = () =>
  root.render(
    <Counter
      value={store.getState()}
      onIncrement={() => store.dispatch({ type: 'INCREMENT' })}
    />,
  );
renderApp();
store.subscribe(renderApp);
