import { Component } from 'react';

class MyIp extends Component {
  state = { ip: '' };

  componentDidMount = () =>
    fetch('/api/ipify?format=json')
      .then((response) => response.json())
      .then(({ ip }) => this.setState({ ip }));

  render = () => (
    <div data-testid="ip">
      {this.state.ip ? `Your IP address is ${this.state.ip}` : 'Loading...'}
    </div>
  );
}

export default MyIp;
