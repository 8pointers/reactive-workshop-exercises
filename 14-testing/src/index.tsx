import { createRoot } from 'react-dom/client';
import './index.css';
import HelloWorld from './1-basic';
import Counter from './2-state-and-events';
import MyIp from './4-async';
import './ex-1';

createRoot(document.getElementById('root-1')).render(<HelloWorld name="World" />);
createRoot(document.getElementById('root-2')).render(<Counter />);
createRoot(document.getElementById('root-3')).render(<MyIp />);
