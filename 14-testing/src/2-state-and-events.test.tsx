import { fireEvent, render, screen } from '@testing-library/react';
import Counter from './2-state-and-events';
import React from 'react';

it('should start counting from 0', () => {
  render(<Counter />);
  expect(screen.getByText(/You clicked/).textContent).toBe('You clicked 0 times');
  expect(document.title).toBe('You clicked 0 times');
});

it('should increment the counter when button is clicked', () => {
  render(<Counter />);
  fireEvent.click(screen.getByText('Click me'));
  expect(screen.getByText(/You clicked/).textContent).toBe('You clicked 1 times');
  expect(document.title).toBe('You clicked 1 times');
});
