import { render, screen } from '@testing-library/react';
import HelloWorld from './1-basic';
import React from 'react';

it('should greet', () => {
  render(<HelloWorld name="World" />);
  expect(screen.getByText(/Hello/).textContent).toBe('Hello World!');
});
