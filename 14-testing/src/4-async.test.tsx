import fetchMock from 'jest-fetch-mock';
import { render, screen } from '@testing-library/react';
import MyIp from './4-async';
import React from 'react';

fetchMock.enableMocks();
fetchMock.dontMock();

it('should fetch and display your outbound IP address', async () => {
  fetchMock.resetMocks();
  fetchMock.mockResponseOnce(JSON.stringify({ ip: '1.2.3.4' }));
  render(<MyIp />);
  expect(screen.getByTestId('ip').textContent).toBe('Loading...');
  expect((await screen.findByText(/Your IP address/)).textContent).toBe(
    'Your IP address is 1.2.3.4',
  );
});
