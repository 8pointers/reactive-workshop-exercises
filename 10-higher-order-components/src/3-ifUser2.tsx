import { createRoot } from 'react-dom/client';

const IfUser = ({ Component, ...props }) => {
  return props.user && <Component {...props} />;
};
const User = ({ user }) => <div>Name: {user.name}</div>;

createRoot(document.getElementById('root-3')!).render(
  <div>
    <IfUser user={{ name: 'Myamoto' }} Component={User} />
    <IfUser user={null} Component={User} />
  </div>,
);
