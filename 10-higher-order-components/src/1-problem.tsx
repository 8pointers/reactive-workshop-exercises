import { createRoot } from 'react-dom/client';

const User = ({ user }) => user && <div>Name: {user.name}</div>;
const Score = ({ score }) => score && <div>Points: {score.points}</div>;

createRoot(document.getElementById('root-1')!).render(
  <div>
    <User user={{ name: 'Myamoto' }} />
    <Score score={{ points: 123 }} />
    <User user={null} />
    <Score score={null} />
  </div>,
);
