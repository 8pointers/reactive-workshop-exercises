import { createRoot } from 'react-dom/client';

const locate = (Component) => (props) => (
  <div style={{ border: 'solid 1px red' }}>
    <Component {...props} />
  </div>
);

let Hello = ({ name }) => <div>Hello {name}!</div>;
Hello = locate(Hello);

createRoot(document.getElementById('root-0')!).render(
  <div>
    <Hello name="World" />
  </div>,
);
