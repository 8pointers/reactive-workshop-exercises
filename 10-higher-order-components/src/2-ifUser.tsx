import { createRoot } from 'react-dom/client';

const IfUser = (Component) => (props) => {
  return props.user && <Component {...props} />;
};
let User = ({ user }) => <div>Name: {user.name}</div>;
User = IfUser(User);

createRoot(document.getElementById('root-2')!).render(
  <div>
    <User user={{ name: 'Myamoto' }} />
    <User user={null} />
  </div>,
);
