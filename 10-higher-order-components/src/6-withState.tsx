import { Component } from 'react';
import { createRoot } from 'react-dom/client';

const withState = (propertyName, methodName, initialValue) => (Comp) =>
  class extends Component<{ name: string }> {
    state = { [propertyName]: initialValue };
    render() {
      const allProps = {
        ...this.props,
        ...this.state,
        [methodName]: (newValue) => this.setState({ [propertyName]: newValue }),
      };
      return <Comp {...allProps} />;
    }
  };

const Player = withState(
  'isVisible',
  'setVisible',
  true,
)(({ name, isVisible, setVisible }) => (
  <div>
    <button onClick={() => setVisible(!isVisible)}>Toggle</button>
    {isVisible && <div>{name}</div>}
  </div>
));

createRoot(document.getElementById('root-6')!).render(<Player name="Myamoto" />);
