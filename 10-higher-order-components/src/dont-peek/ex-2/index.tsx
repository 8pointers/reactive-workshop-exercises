import React, { Component } from 'react';
import { createRoot } from 'react-dom/client';

const toggleable =
  <Props,>() =>
  (AComponent) => {
    return class Toggleable extends Component<Props, { isOn: boolean }> {
      state = { isOn: true };

      toggle = () => {
        this.setState(({ isOn }) => ({ isOn: !isOn }));
      };

      render() {
        return (
          <AComponent isOn={this.state.isOn} toggle={this.toggle} {...this.props} />
        );
      }
    };
  };

const User = toggleable<{ user: { name: string } }>()(({ isOn, toggle, user }) => (
  <div>
    <button onClick={toggle}>Toggle</button>
    {isOn && user.name}
  </div>
));
const Account = toggleable<{ account: { balance: number } }>()(
  ({ isOn, toggle, account }) => (
    <div>
      <button onClick={toggle}>Toggle</button>
      {isOn && account.balance}
    </div>
  )
);

createRoot(document.getElementById('root-ex-2')!).render(
  <div>
    <User user={{ name: 'Myamoto' }} />
    <Account account={{ balance: 123 }} />
  </div>
);
