import { Component } from 'react';
import { createRoot } from 'react-dom/client';

const autoRefresh =
  <Props,>(period = 1000) =>
  (MyComponent) =>
    class AutoRefresh extends Component<Props> {
      interval: any;
      componentDidMount() {
        this.interval = setInterval(() => this.forceUpdate(), period);
      }

      componentWillUnmount = () => clearInterval(this.interval);

      render = () => <MyComponent {...this.props} />;
    };

const CurrentTime = ({ label }) => (
  <div>
    {label}
    {new Date().toLocaleTimeString()}
  </div>
);
const OneSecondClock = autoRefresh<{ label: string }>()(CurrentTime);
const TwoSecondClock = autoRefresh<{ label: string }>(2000)(CurrentTime);

createRoot(document.getElementById('root-ex-1')!).render(
  <div>
    <OneSecondClock label="One: " />
    <TwoSecondClock label="Two: " />
  </div>,
);

export default autoRefresh;
