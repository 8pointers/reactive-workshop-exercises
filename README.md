# Reactive Workshop Exercises

## Prerequisites

- Docker
- Visual Studio Code with [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension installed

## Running the apps

Open new Visual Studio Code window and use the _Remote-Containers: Clone Repository in Container Volume..._ command to clone this ([https://bitbucket.org/8pointers/reactive-workshop-exercises.git](https://bitbucket.org/8pointers/reactive-workshop-exercises.git)) repository. Once the workspace is ready, open a new Visual Studio Code terminal and run:

```sh
cd 01-intro
npm start
```
