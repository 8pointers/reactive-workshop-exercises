import { createRoot } from 'react-dom/client';

import './index.css';
import LoginForm from './1-input';
import FeedbackForm from './2-textarea';
import ThemeSelectionForm from './3-select';
import LoginForm2 from './4-computed-property-name-trick';
import LoginFormWithHooks from './5-hooks';
import LoginFormWithCustomHook from './6-hooks';

import './ex-1';

createRoot(document.getElementById('root-1')!).render(<LoginForm />);

createRoot(document.getElementById('root-2')!).render(<FeedbackForm />);

createRoot(document.getElementById('root-3')!).render(<ThemeSelectionForm />);

createRoot(document.getElementById('root-4')!).render(<LoginForm2 />);

createRoot(document.getElementById('root-5')!).render(<LoginFormWithHooks />);

createRoot(document.getElementById('root-6')!).render(<LoginFormWithCustomHook />);
