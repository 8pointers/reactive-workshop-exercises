import { ChangeEvent, FormEvent, useState } from 'react';

const LoginForm = () => {
  const [username, setUsername] = useState('');
  const login = (event: FormEvent) => {
    alert(`Submitted username: ${username}`);
    event.preventDefault();
  };
  const onChange = (e: ChangeEvent<HTMLInputElement>) => setUsername(e.target.value);
  return (
    <form onSubmit={login}>
      <label>
        Username:
        <input type="text" value={username} onChange={onChange} />
      </label>
      <input type="submit" value="Login" />
    </form>
  );
};

export default LoginForm;
