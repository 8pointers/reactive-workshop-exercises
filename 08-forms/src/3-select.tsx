import { ChangeEvent, FormEvent, useState } from 'react';

const ThemeSelectionForm = () => {
  const [theme, setTheme] = useState('dark');
  const submit = (event: FormEvent) => {
    alert(`Selected theme: ${theme}`);
    event.preventDefault();
  };
  const onChange = (event: ChangeEvent<HTMLSelectElement>) =>
    setTheme(event.target.value);
  return (
    <form onSubmit={submit}>
      <label>
        Theme:
        <select value={theme} onChange={onChange}>
          <option value="light">Light</option>
          <option value="dark">Dark</option>
        </select>
      </label>
      <input type="submit" value="Set" />
    </form>
  );
};

export default ThemeSelectionForm;
