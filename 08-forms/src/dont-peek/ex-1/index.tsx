import { useState, useRef, FormEvent } from 'react';
import { createRoot } from 'react-dom/client';
import classnames from 'classnames';
import './style.css';

const filters: { [key: string]: (f: { isRemoved?: boolean }) => boolean } = {
  all: (_: { isRemoved?: boolean }) => true,
  removed: (friend: { isRemoved?: boolean }) => friend.isRemoved || false,
  notRemoved: (friend: { isRemoved?: boolean }) => !friend.isRemoved,
};

const Greetings = () => {
  const [friends, setFriends] = useState([
    { name: 'First', id: 0 },
    { name: 'Second', isRemoved: true, id: 1 },
    { name: 'Third', id: 2 },
    { name: 'Fourth', isRemoved: true, id: 3 },
  ]);
  const [name, setName] = useState('');
  const [filter, setFilter] = useState('all');
  const id = useRef(friends.length);
  const add = (event: FormEvent) => {
    event.preventDefault();
    setFriends([...friends, { name, id: id.current++ }]);
    setName('');
  };
  const remove = (id: number) =>
    setFriends(
      friends.map((friend) =>
        friend.id === id ? { ...friend, isRemoved: !friend.isRemoved } : friend
      )
    );
  return (
    <div>
      {Object.keys(filters).map((f) => (
        <span
          className={classnames({ selected: f === filter })}
          onClick={() => setFilter(f)}
        >
          {f}{' '}
        </span>
      ))}
      <form onSubmit={add}>
        <label>
          Name:
          <input
            type="text"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </label>
        <input type="submit" value="Add" />
      </form>
      {friends.length
        ? friends.filter(filters[filter]).map(({ name, id, isRemoved }) => {
            return (
              <div
                key={id}
                className={classnames({ removed: isRemoved })}
                onClick={() => remove(id)}
              >
                {name}
              </div>
            );
          })
        : 'No friends :('}
    </div>
  );
};

createRoot(document.getElementById('root-ex-1')!).render(<Greetings />);
