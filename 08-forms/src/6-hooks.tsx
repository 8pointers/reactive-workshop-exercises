import { ChangeEvent, FormEvent, useState } from 'react';
const useFormInput = (initialValue: string) => {
  const [value, setValue] = useState(initialValue);
  return {
    value,
    onChange: (event: ChangeEvent<HTMLInputElement>) => setValue(event.target.value),
  };
};
const LoginForm = () => {
  const [username, password] = ['', ''].map(useFormInput);
  const login = (event: FormEvent) => {
    alert(`Submitted ${username.value}:${password.value}`);
    event.preventDefault();
  };
  return (
    <form onSubmit={login}>
      Username: <input type="text" {...username} />
      Password: <input type="password" {...password} />
      <input type="submit" value="Login" />
    </form>
  );
};
export default LoginForm;
