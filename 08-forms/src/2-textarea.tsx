import { ChangeEvent, FormEvent, useState } from 'react';

const FeedbackForm = () => {
  const [feedback, setFeedback] = useState('Your opinion matters to us...');
  const postFeedback = (event: FormEvent) => {
    alert(`Sending feedback: ${feedback}`);
    event.preventDefault();
  };
  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) =>
    setFeedback(e.target.value);
  return (
    <form onSubmit={postFeedback}>
      <label>
        Feedback:
        <textarea value={feedback} onChange={onChange} />
      </label>
      <input type="submit" value="Send" />
    </form>
  );
};

export default FeedbackForm;
