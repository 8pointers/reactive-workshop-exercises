import { createRoot } from 'react-dom/client';
import { createSlice, configureStore } from '@reduxjs/toolkit';
import { useSelector, useDispatch, Provider } from 'react-redux';

const loggingMiddleware = (store) => (next) => (action) => {
  const previousState = store.getState();
  let result = next(action);
  console.log(previousState, action, store.getState());
  return result;
};
const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
  },
});
const { increment } = counterSlice.actions;
const store = configureStore({
  reducer: {
    counter: counterSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(loggingMiddleware),
});

const Counter = () => {
  const count = useSelector<{ counter: { value: number } }, number>((state) => {
    console.log(state);
    return state.counter.value;
  });
  const dispatch = useDispatch();
  return <button onClick={() => dispatch(increment())}>{count}</button>;
};

createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <Counter />
  </Provider>
);
