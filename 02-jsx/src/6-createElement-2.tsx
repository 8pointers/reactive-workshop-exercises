import { createElement } from 'react';
import { createRoot } from 'react-dom/client';

const Child = () => createElement('div', null, 'Hello World!');

const Parent = () =>
  createElement(
    'div',
    null,
    createElement(Child, null),
    createElement('input', { type: 'button', value: 'Click me' }),
  );

createRoot(document.getElementById('root-6-2')!).render(createElement(Parent, null));
