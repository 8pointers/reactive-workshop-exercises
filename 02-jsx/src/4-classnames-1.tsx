import { createRoot } from 'react-dom/client';

const loggedIn = true;
const vip = false;
const error = 'An error occurred.';
const HelloWorld = () => {
  let classes = '';
  if (loggedIn) {
    classes += ' loggedIn';
  }
  if (vip) {
    classes += ' vip';
  }
  if (error) {
    classes += ' error';
  }
  return <div className={classes}>Hello!</div>;
};

createRoot(document.getElementById('root-4-1')!).render(<HelloWorld />);
