import { createRoot } from 'react-dom/client';

const Child = () => <div>Hello World!</div>;

const Parent = () => (
  <div>
    <Child />
    <input type="button" value="Click me" />
  </div>
);

createRoot(document.getElementById('root-6-1')!).render(<Parent />);
