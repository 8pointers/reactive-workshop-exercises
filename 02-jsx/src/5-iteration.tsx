import { createRoot } from 'react-dom/client';

const Greetings = () => {
  return (
    <div>
      {['Myamoto', 'Hattori', 'Dave'].map((name) => (
        <div>Hello {name}</div>
      ))}
    </div>
  );
};

createRoot(document.getElementById('root-5')!).render(<Greetings />);
