import { createRoot } from 'react-dom/client';

const HelloWorld = () => {
  const name = 'World';
  return <div>Hello {name}!</div>;
};

createRoot(document.getElementById('root-1')!).render(<HelloWorld />);
