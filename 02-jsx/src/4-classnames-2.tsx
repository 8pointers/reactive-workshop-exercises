import { createRoot } from 'react-dom/client';
import classnames from 'classnames';

const loggedIn = true;
const vip = false;
const error = 'An error occurred.';
const HelloWorld = () => (
  <div className={classnames({ loggedIn, vip, error })}>Hello!</div>
);

createRoot(document.getElementById('root-4-2')!).render(<HelloWorld />);
