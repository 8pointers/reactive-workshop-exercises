import { ReactNode } from 'react';
import { createRoot } from 'react-dom/client';

const RandomGreeting = () => {
  let result: ReactNode;
  if (Math.random() < 0.5) {
    result = <div>Hello!</div>;
  } else {
    result = <div>Howdy!</div>;
  }
  return result;
};

createRoot(document.getElementById('root-2')!).render(<RandomGreeting />);
