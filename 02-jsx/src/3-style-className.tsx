import { createRoot } from 'react-dom/client';

const HelloWorld = () => {
  const styleObject = {
    backgroundColor: 'grey',
    color: 'blue',
  };
  return (
    <div className="error" style={styleObject}>
      Hello!
    </div>
  );
};

createRoot(document.getElementById('root-3')!).render(<HelloWorld />);
