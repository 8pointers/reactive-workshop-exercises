import PropTypes from 'prop-types';
import { createRoot } from 'react-dom/client';

const Hello = ({ name }: { name: string }) => <div>Hello {name}!</div>;

Hello.propTypes = {
  name: PropTypes.string.isRequired,
};

createRoot(document.getElementById('root-4')!).render(<Hello name="World" />);
