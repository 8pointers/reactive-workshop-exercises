import { createRoot } from 'react-dom/client';
import GameOfLife from './game-of-life';

createRoot(document.getElementById('root-ex-2')!).render(<GameOfLife />);
