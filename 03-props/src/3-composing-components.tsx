const Hello = (props: { name: string }) => <div>Hello {props.name}!</div>;

const Greetings = () => (
  <div>
    <Hello name="Myamoto" />
    <Hello name="Hattori" />
    <Hello name={'Dave'} />
  </div>
);

export default Greetings;
