import { createRoot } from 'react-dom/client';

import './index.css';

import './1-basic';
import './2-class-components';
import Greetings from './3-composing-components';
import './4-propTypes';
import './5-defaultProps';

import './ex-2/index';

createRoot(document.getElementById('root-3')!).render(<Greetings />);
