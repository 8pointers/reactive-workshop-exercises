import { createRoot } from 'react-dom/client';

const Hello = (props: {
  name: string;
  age: number;
  address: { street: string; postcode: string };
}) => (
  <div>
    Hello {props.name} from {props.address.street} ! You are {props.age} years old.
  </div>
);

createRoot(document.getElementById('root-1')!).render(
  <Hello
    name="World"
    age={23}
    address={{ street: 'Ninja Way', postcode: 'XXX-YY' }}
  />,
);
