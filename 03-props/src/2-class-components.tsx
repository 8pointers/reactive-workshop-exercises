import { Component } from 'react';
import { createRoot } from 'react-dom/client';

class Hello extends Component<{ name: string }> {
  render() {
    return <div>Hello {this.props.name}!</div>;
  }
}

createRoot(document.getElementById('root-2')!).render(<Hello name="World" />);
