import PropTypes from 'prop-types';
import { createRoot } from 'react-dom/client';

const Hello = ({ name }: { name: string }) => <div>Hello {name}!</div>;

Hello.propTypes = {
  name: PropTypes.string,
};

Hello.defaultProps = {
  name: 'World',
};

createRoot(document.getElementById('root-5')!).render(
  <div>
    <Hello name="Myamoto" />
    <Hello />
  </div>,
);
