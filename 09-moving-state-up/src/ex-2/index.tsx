import { createRoot } from 'react-dom/client';
import GameOfLife from './game-of-life';

createRoot(document.getElementById('root-ex-2')!).render(
  <GameOfLife n={10} width={20} height={20} />,
);
