import { Component } from 'react';

type Props = { value: number; onIncrement: () => void };
const Counter = ({ value, onIncrement }: Props) => (
  <button onClick={onIncrement}>{value}</button>
);

class CounterContainer extends Component<{}, { value: number }> {
  state = { value: 0 };
  increment = () => this.setState(({ value }) => ({ value: value + 1 }));
  render = () => <Counter value={this.state.value} onIncrement={this.increment} />;
}

export default CounterContainer;
