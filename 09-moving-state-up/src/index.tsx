import React from 'react';
import { createRoot } from 'react-dom/client';

import './index.css';

import Counters from './1-problem';
import Counters2 from './2-move-on-up';
import CounterAsContainer from './3-container';
import CounterWithProps from './4-1-context-motivation';
import CounterWithContext from './4-context';
import CounterWithUseContext from './5-useContext';

import './ex-2';
import './ex-3';

createRoot(document.getElementById('root-1')!).render(<Counters />);
createRoot(document.getElementById('root-2')!).render(<Counters2 />);
createRoot(document.getElementById('root-3')!).render(<CounterAsContainer />);
createRoot(document.getElementById('root-4-1')!).render(<CounterWithProps />);
createRoot(document.getElementById('root-4')!).render(<CounterWithContext />);
createRoot(document.getElementById('root-5')!).render(<CounterWithUseContext />);
