import { createRoot } from 'react-dom/client';
import { BrowserRouter, Link, Route, Routes, useParams } from 'react-router-dom';

const User = () => {
  const { id } = useParams();
  return <div>This is User with id: {id}</div>;
};

const App = () => (
  <BrowserRouter>
    <div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/user/123">User 123</Link>
        </li>
        <li>
          <Link to="/user/456">User 456</Link>
        </li>
      </ul>
      <Routes>
        <Route path="/" element=<div>This is Home.</div> />
        <Route path="/about" element=<div>This is About.</div> />
        <Route path="/user/:id" element=<User /> />
      </Routes>
    </div>
  </BrowserRouter>
);

createRoot(document.getElementById('root-1')!).render(<App />);
