import { useState } from 'react';
import { createRoot } from 'react-dom/client';

const Problem = () => {
  const [number, setNumber] = useState(0);
  const inc1 = () => setNumber(number + 1);
  const dec1 = () => setNumber(number - 1);
  const inc2 = () => setNumber((number) => number + 1);
  const dec2 = () => setNumber((number) => number - 1);
  const [{ action }, setAction] = useState({ action: () => setNumber(123) });
  return (
    <div>
      {number}
      <button onClick={() => setAction({ action: inc1 })}>++ 🤕</button>
      <button onClick={() => setAction({ action: dec1 })}>-- 🤕</button>
      <button onClick={() => setAction({ action: inc2 })}>++ 😀</button>
      <button onClick={() => setAction({ action: dec2 })}>-- 😀</button>
      <button onClick={action}>!</button>
    </div>
  );
};

createRoot(document.getElementById('root-1-4')!).render(<Problem />);
