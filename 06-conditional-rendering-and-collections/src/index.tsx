import { createRoot } from 'react-dom/client';

import './index.css';

import GameIfElse from './1-if-else';
import GameTernary1 from './2-ternary';
import GameTernary2 from './3-ternary-inlined';
import Players from './4-problem';
import PlayersWithKey from './5-key';
import './6-incorrect-key-usage';
import './7-correct-key-usage';
import MoreElaboratePlayers from './8-problem';

createRoot(document.getElementById('root-1')!).render(<GameIfElse />);
createRoot(document.getElementById('root-2')!).render(<GameTernary1 />);
createRoot(document.getElementById('root-3')!).render(<GameTernary2 />);

createRoot(document.getElementById('root-4')!).render(<Players />);
createRoot(document.getElementById('root-5')!).render(<PlayersWithKey />);
createRoot(document.getElementById('root-8')!).render(<MoreElaboratePlayers />);
