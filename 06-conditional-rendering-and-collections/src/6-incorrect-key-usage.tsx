import { createRoot } from 'react-dom/client';

const Hello = ({ id, name }: { id: number; name: string }) => (
  <div key={id}>{name}</div>
);

const Greetings = () => (
  <div>
    {['Myamoto', 'Hattori', 'Dave'].map((name, i) => (
      <Hello id={i} name={name} />
    ))}
  </div>
);

createRoot(document.getElementById('root-6')!).render(<Greetings />);
