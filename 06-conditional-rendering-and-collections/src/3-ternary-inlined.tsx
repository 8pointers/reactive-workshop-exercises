import { useState } from 'react';

const Congratulations = () => <div>Congratulations!</div>;

const Comiserations = () => <div>Better luck next time.</div>;

const Game = () => {
  const [state, setState] = useState<string>();
  const playAgain = () => setState(Math.random() < 0.5 ? 'won' : 'lost');
  return (
    <div>
      <button onClick={playAgain}>Play</button>
      {state && (state === 'won' ? <Congratulations /> : <Comiserations />)}
    </div>
  );
};

export default Game;
