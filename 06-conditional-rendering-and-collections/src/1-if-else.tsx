import { useState } from 'react';

const Congratulations = () => <div>Congratulations!</div>;

const Comiserations = () => <div>Better luck next time.</div>;

const Game = () => {
  const [state, setState] = useState<string>();
  const playAgain = () => setState(Math.random() < 0.5 ? 'won' : 'lost');
  let message;
  if (state === 'won') {
    message = <Congratulations />;
  } else if (state === 'lost') {
    message = <Comiserations />;
  }
  return (
    <div>
      <button onClick={playAgain}>Play</button>
      {message}
    </div>
  );
};

export default Game;
