import { createRoot } from 'react-dom/client';

const Hello = ({ name }: { name: string }) => <div>{name}</div>;

const Greetings = () => (
  <div>
    {['Myamoto', 'Hattori', 'Dave'].map((name, i) => (
      <Hello key={i} name={name} />
    ))}
  </div>
);

createRoot(document.getElementById('root-7')!).render(<Greetings />);
