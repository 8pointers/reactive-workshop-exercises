import { useState } from 'react';

const Players = () => {
  const [players, setPlayers] = useState(
    Array.from({ length: 5 }, (_, i) => ({ id: i, name: `player-${i}` })),
  );
  const remove = (id: number) =>
    setPlayers(players.filter((player) => player.id !== id));
  return (
    <div>
      {players.map(({ id, name }) => (
        <div key={id} onClick={() => remove(id)}>
          {name}
        </div>
      ))}
    </div>
  );
};

export default Players;
