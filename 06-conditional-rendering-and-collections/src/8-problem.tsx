import { useState } from 'react';

const Player = ({ name, remove }) => {
  const [isVisible, setIsVisible] = useState(true);
  return (
    <div>
      <button onClick={remove}>Remove</button>
      <button onClick={() => setIsVisible(!isVisible)}>Toggle</button>
      {isVisible && name}
    </div>
  );
};
const Players = () => {
  const [players, setPlayers] = useState(() =>
    Array.from({ length: 5 }, (e, i) => ({
      id: i,
      name: `player-${i + 1}`,
    })),
  );
  const remove = (id: number) =>
    setPlayers(players.filter((player) => player.id !== id));
  return (
    <div>
      Using index as a key:
      <div>
        {players.map(({ id, name }, index) => (
          <Player key={index} name={name} remove={() => remove(id)} />
        ))}
      </div>
      Using id as key:
      <div>
        {players.map(({ id, name }) => (
          <Player key={id} name={name} remove={() => remove(id)} />
        ))}
      </div>
    </div>
  );
};
export default Players;
