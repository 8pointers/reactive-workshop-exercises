import { combineLatest, timer } from 'rxjs';
import { map, take } from 'rxjs/operators';

const bmi = combineLatest(
  [
    timer(0, 100).pipe(
      take(75),
      map((year) => Math.min(3 + year * 4, 70))
    ),
    timer(0, 100).pipe(
      take(75),
      map((year) => Math.min(0.55 + year * 0.08, 1.8))
    ),
  ],
  (weight, height) => weight / (height * height)
);

bmi.subscribe((bmi) => console.log('BMI', bmi));
