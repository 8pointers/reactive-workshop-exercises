import { from, fromEvent, merge, timer } from 'rxjs';
import { mergeMap, throttleTime } from 'rxjs/operators';

const myIp = merge(
  timer(0, 10000),
  fromEvent(document.getElementById('refresh'), 'click')
).pipe(
  throttleTime(1000),
  mergeMap(() =>
    from(fetch('https://api.ipify.org').then((response) => response.text()))
  )
);

myIp.subscribe((result) => console.log('myIp', result));
